# Gene Regulation Network

BioNLP-ST 2013 Gene Regulation Network (GRN) dataset.

This repository contains training, development and test sets for the GRN task.

Information about the GRN task: https://2013.bionlp-st.org/tasks/gene-regulation-network-grn

License: see `LICENSE`.

Contact: Robert.Bossy@inrae.fr
